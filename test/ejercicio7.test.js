// var ejer = require('../ejercicios/ejercicio7/ejercicio7')
// var mocha = require('mocha');
// var chai = require('chai');
// var expect = chai.expect;
// var sinon = require('sinon');
//
// describe('Ejercicio 7', function(){
//     describe('Return the first 10001 prime numbers', function(){
//        it('should export a function', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.get_prime_numbers).to.be.a('function');
//             done();
//        });
//
//        it('should return a number', function(done){
//            this.timeout(0)
//            var ejercicio = new ejer();
//            expect(ejercicio.get_prime_numbers()).to.be.an('number');
//            done();
//        });
//     });
//
//     describe('Is a prime number?', function(){
//         it('should export a function',function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.is_prime).to.be.a('function');
//             done();
//         });
//
//         it('should receive a number', function(done){
//             this.timeout(0);
//             var ejercicio = new ejer();
//             var spy = sinon.spy(ejercicio, 'is_prime');
//             ejercicio.get_prime_numbers();
//             expect(spy.getCall(0).args[0]).to.be.a('number');
//             done();
//         })
//
//         it('should return a boolean', function(done){
//             var ejercicio = new ejer();
//             var spy = sinon.spy(ejercicio, 'is_prime')
//             ejercicio.is_prime(20);
//             expect(spy.getCall(0).args[0]).to.be.a('number');
//             done();
//         })
//     })
// })