// var ejer = require('../ejercicios/ejercicio14/ejercicio14');
// var mocha = require('mocha');
// var chai = require('chai');
// var expect = chai.expect;
// var sinon = require('sinon');
//
// describe('Ejercicio 14', function(){
//     var ejercicio;
//     beforeEach(function(){
//         ejercicio = new ejer()
//     })
//
//     describe('Operator',function(){
//         it('should export a function', function(done){
//             expect(ejercicio.operator).to.be.a('function');
//             done();
//         })
//         it('should receive a number', function(done){
//             this.timeout(0)
//             var spy = sinon.spy(ejercicio, 'operator');
//             ejercicio.iterator();
//             expect(spy.getCall(8).args[0]).to.be.a('number')
//             done();
//         })
//         it('should return a number', function(done){
//             expect(ejercicio.operator(3)).to.be.an('number');
//             done();
//         })
//     })
//     describe('iterator', function(){
//         it('should export a function', function(done){
//             expect(ejercicio.iterator).to.be.a('function');
//             done();
//         })
//         it('should return a number', function(done){
//             this.timeout(0)
//             expect(ejercicio.iterator()).to.be.a('number');
//             done();
//         })
//     })
// })