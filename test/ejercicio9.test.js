// var ejer = require('../ejercicios/ejercicio9/ejercicio9');
// var mocha = require('mocha');
// var chai = require('chai');
// var expect = chai.expect;
// var sinon = require('sinon');
//
// describe('Ejercicio9', function(){
//     describe('Get pythagorean triplet', function(){
//        it('should export a function', function(done){
//            var ejercicio = new ejer();
//            expect(ejercicio.get_pythagorean_triplet).to.be.a('function');
//            done();
//        });
//        it('should return an array', function(done){
//            var ejercicio = new ejer();
//            expect(ejercicio.get_pythagorean_triplet()).to.be.an('array');
//            done();
//        })
//     });
//     describe('Get product', function(){
//         it('should export a function', function(done){
//             var ejercicio =new ejer();
//             expect(ejercicio.get_product).to.be.a('function');
//             done();
//         });
//         it('should receive an array', function(done){
//             var ejercicio = new ejer();
//             var spy = sinon.spy(ejercicio, 'get_product');
//             var a_numbers = ejercicio.get_pythagorean_triplet();
//             ejercicio.get_product(a_numbers);
//             expect(spy.getCall(0).args[0]).to.be.an('array');
//             done();
//         })
//
//         it('should return a number', function(done){
//             var ejercicio = new ejer();
//             var a_numbers = ejercicio.get_pythagorean_triplet();
//             expect(ejercicio.get_product(a_numbers)).to.be.a('number');
//             done();
//         })
//     })
// })