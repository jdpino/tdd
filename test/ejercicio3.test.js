// var ejer = require('../ejercicios/ejercicio3/ejercicio3')
// var mocha = require('mocha');
// var chai = require('chai');
// var expect = chai.expect;
// var sinon = require('sinon');
//
// describe('Ejercicio 3', function(){
//     describe('Get all prime numbers', function(){
//         it('Should export a function', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.get_prime_numbers).to.be.a('function');
//             done();
//         });
//
//         it('Should return an array', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.get_prime_numbers()).to.be.an('array');
//             done();
//         });
//     });
//
//     describe('Get largest number', function(){
//         it('Should export a function', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.get_number).to.be.a('function');
//             done();
//         })
//
//         it('Should receive an array', function(done){
//             var ejercicio = new ejer();
//             var spy = sinon.spy(ejercicio, 'get_number');
//             var n_primos = ejercicio.get_prime_numbers();
//             ejercicio.get_number(n_primos);
//             expect(spy.getCall(0).args[0]).to.be.an('array');
//             done();
//         })
//
//         it('Should return a number', function(done){
//             var ejercicio = new ejer();
//             var n_primos = ejercicio.get_prime_numbers();
//             expect(ejercicio.get_number(n_primos)).to.be.a('number');
//             done();
//         });
//     })
// });
