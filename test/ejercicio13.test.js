// var ejer = require('../ejercicios/ejercicio13/ejercicio13');
// var mocha = require('mocha');
// var chai = require('chai');
// var expect = chai.expect;
// var sinon = require('sinon');
//
// describe('Ejercicio 13', function(){
//     describe('Sum', function(){
//         it('should export a function', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.sum).to.be.a('function');
//             done();
//         })
//         it('should return a string', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.sum()).to.be.a('string');
//             done();
//         })
//     })
//     describe('Truncate', function(){
//         it('should export a function', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.truncate).to.be.a('function');
//             done();
//         })
//         it('should receive a string', function (done) {
//             var ejercicio = new ejer();
//             var spy = sinon.spy(ejercicio, 'truncate');
//             var n_number = ejercicio.sum();
//             ejercicio.truncate(n_number);
//             expect(spy.getCall(0).args[0]).to.be.a('string');
//             done();
//         })
//         it('should return a number', function(done){
//             var ejercicio = new ejer();
//             var n_number = ejercicio.sum();
//             expect(ejercicio.truncate(n_number)).to.be.a('number');
//             done();
//         })
//     })
//
// });