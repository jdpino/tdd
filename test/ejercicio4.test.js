// var ejer = require('../ejercicios/ejercicio4/ejercicio4')
// var mocha = require('mocha');
// var chai = require('chai');
// var expect = chai.expect;
// var sinon = require('sinon')
//
// describe('Ejercicio 4', function(){
//     describe('Check if a number is palindrome',function(){
//         it('Should export a function', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.is_palindrome).to.be.a('function')
//             done();
//         });
//         it('should receive a number', function(done){
//             var ejercicio = new ejer();
//             var spy = sinon.spy(ejercicio, 'is_palindrome');
//             ejercicio.is_palindrome(100*100)
//             console.log(ejercicio.is_palindrome(100*100))
//             expect(spy.getCall(0).args[0]).to.be.a('number');
//             done();
//         });
//         it('Should return a boolean', function(done){
//             this.timeout(0)
//             var ejercicio = new ejer();
//             expect(ejercicio.is_palindrome(100*100)).to.be.a('boolean');
//             done();
//         });
//     });
//     describe('Iterator', function(){
//        it('should export a function', function(done){
//            var ejercicio = new ejer();
//            expect(ejercicio.iterator).to.be.a('function')
//            done();
//        });
//        it('should return a number', function(done){
//            this.timeout(0)
//            var ejercicio = new ejer();
//            expect(ejercicio.iterator(55555)).to.be.a('number');
//            console.log(ejercicio.iterator())
//            done();
//        });
//     });
// })