// var ejer = require('../ejercicios/ejercicio10/ejercicio10');
// var mocha = require('mocha');
// var chai = require('chai')
// var expect = chai.expect;
// var sinon = require('sinon')
//
// describe('Ejercicio 10', function(){
//     describe('Get summatory', function() {
//         it('should export a function', function(done) {
//             var ejercicio = new ejer();
//             expect(ejercicio.summatory).to.be.a('function');
//             done();
//         });
//
//         it('should return a number', function(done){
//             this.timeout(0);
//             var ejercicio = new ejer();
//             expect(ejercicio.summatory()).to.be.a('number');
//             console.log(ejercicio.summatory())
//             done();
//         });
//     });
//     describe('Is prime', function(){
//         it('should export a function', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.is_prime).to.be.a('function')
//             done();
//         });
//
//         it('should receive a number', function(done){
//             var ejercicio = new ejer();
//             var spy = sinon.spy(ejercicio, 'is_prime');
//             ejercicio.is_prime(97)
//             expect(spy.getCall(0).args[0]).to.be.a('number')
//             done();
//         });
//
//         it('should return a boolean', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.is_prime(34)).to.be.a('boolean');
//             done();
//         });
//
//
//     });
// });