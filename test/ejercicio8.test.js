// var ejer = require('../ejercicios/ejercicio8/ejercicio8')
// var mocha = require('mocha');
// var chai = require('chai');
// var expect = chai.expect;
// var sinon = require('sinon');
//
// describe('Ejercicio 8', function(){
//     describe('Get 13 consecutive digits',function(){
//         it('should export a function', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.get_digits).to.be.a('function');
//             done();
//         });
//
//         it('should return an array', function(done){
//            var ejercicio = new ejer();
//            expect(ejercicio.get_digits()).to.be.an('array');
//            done();
//         });
//
//     });
//     describe('Summatory', function(){
//         it('should export a function', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.summatory).to.be.a('function');
//             done();
//         });
//
//         it('should receive an array', function(done){
//             this.timeout(5000)
//             var ejercicio = new ejer();
//             var spy = sinon.spy(ejercicio, 'summatory');
//             ejercicio.get_digits();
//             expect(spy.getCall(0).args[0]).to.be.an('array');
//             done();
//         })
//
//         it('should return a number', function(done){
//             this.timeout(5000);
//             var ejercicio = new ejer();
//             expect(ejercicio.summatory([1,2,3,4,5,6,7,8,9,0])).to.be.an('number');
//             done();
//         });
//     });
//     describe('Get the largest number', function(){
//         it('should export a function', function(done){
//            var ejercicio = new ejer();
//            expect(ejercicio.get_largest).to.be.a('function');
//            done();
//         });
//         it('should receive an array', function(done){
//            var ejercicio = new ejer();
//            var spy = sinon.spy(ejercicio,'get_largest');
//            var a_numbers = ejercicio.get_digits();
//            ejercicio.get_largest(a_numbers);
//            expect(spy.getCall(0).args[0]).to.be.an('array');
//            done();
//         });
//         it('should return a number', function(done){
//             var ejercicio = new ejer();
//             var a_numbers = ejercicio.get_digits();
//             expect(ejercicio.get_largest(a_numbers)).to.be.a('number');
//             done();
//         })
//         it('should be positive and greater than 0', function(done){
//             var ejercicio = new ejer();
//             var a_numbers = ejercicio.get_digits();
//             expect(ejercicio.get_largest(a_numbers)).to.be.above(0);
//             done();
//         })
//     });
// });