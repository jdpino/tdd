// var ejer = require('../ejercicios/ejercicio2/ejercicio2');
// var mocha = require('mocha');
// var chai = require('chai');
// var expect = chai.expect;
// var sinon = require('sinon');
//
// describe('Ejercicio 2', function () {
//     describe('Get Fibonacci numbers', function(){
//
//         it('should export a function', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.getFibonacciNumbers).to.be.a('function');
//             done();
//         })
//
//         it('should return an array', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.getFibonacciNumbers()).to.be.an('array')
//             done();
//         })
//
//         it('returned array should have a length of 32', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.getFibonacciNumbers().length).eqls(32)
//             done();
//         })
//
//     })
//
//     describe('Get summatory', function(){
//         it('should export a function', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.get_sum).to.be.a('function');
//             done();
//         })
//         it('should receive an array', function(done){
//             var ejercicio = new ejer();
//             var spy = sinon.spy(ejercicio, 'get_sum');
//             var a_numeros = ejercicio.getFibonacciNumbers();
//             ejercicio.get_sum(a_numeros);
//             expect(spy.getCall(0).args[0]).to.be.an('array')
//             done();
//         })
//         it('should return a number', function(done){
//             var ejercicio = new ejer();
//             var a_numeros = ejercicio.getFibonacciNumbers();
//             expect(ejercicio.get_sum(a_numeros)).to.be.a('number')
//             done();
//         })
//
//
//     })
// })