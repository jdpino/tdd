// var ejer = require('../ejercicios/ejercicio6/ejercicio6')
// var mocha = require('mocha');
// var chai = require('chai');
// var expect = chai.expect;
//
// describe('Ejercicio 6', function(){
//     describe('Get the sum of squares', function(){
//         it('should export a function', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.get_sum_squares).to.be.a('function');
//             done();
//         });
//
//         it('should return a number', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.get_sum_squares()).to.be.a('number');
//             done();
//         });
//     });
//
//     describe('Get the square of sum', function(){
//         it('should export a function', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.get_square_sum).to.be.a('function');
//             done();
//         });
//
//         it('should return a number', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.get_square_sum()).to.be.a('number');
//             done();
//         });
//
//     });
//
//     describe('Difference between the sum of the squares and the square of the sum', function(){
//         it('should export a function', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.get_difference).to.be.a('function');
//             done();
//         })
//
//         it('should return a number', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.get_difference()).to.be.a('number');
//             done();
//         });
//
//         it('should return a positive number', function(done){
//             var ejercicio = new ejer();
//             expect(ejercicio.get_difference()).to.be.above(0)
//             done();
//         })
//
//     })
// })