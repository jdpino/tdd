function Ejercicio5(){};

Ejercicio5.prototype.get_multiple = function() {
    //Crea un array con todos los números
    function range(min, max) {
        var arr = [];
        for (var i = min; i <= max; i++) {
            arr.push(i);
        }
        return arr;
    }

    function gcd(a, b) {
        return !b ? a : gcd(b, a % b);
    }

    function lcm(a, b) {
        return (a * b) / gcd(a, b);
    }

    var multiple = 1;
    range(1, 20).forEach(function(n) {
        multiple = lcm(multiple, n);
    });
    return multiple;
}

module.exports = Ejercicio5;