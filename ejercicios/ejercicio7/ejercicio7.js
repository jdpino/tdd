function Ejercicio7(){}

Ejercicio7.prototype.get_prime_numbers = function(){
    var n_number = 0;
    for(var i=0, count=0; count < 10001; i++){
        if (this.is_prime(i)){
            // console.log(count+"->"+i)
            if (i > n_number){
                n_number = i
            }
            ++count;
        }
    }
    return n_number;
}

Ejercicio7.prototype.is_prime = function(number){
    var result = true;
    for (var i= 2; i < number; i++){
        if (number%i == 0 ){
            result = false;
            break;
        }
    }
    if(number <= 1){
        result = false
    }
    return result
}

module.exports = Ejercicio7