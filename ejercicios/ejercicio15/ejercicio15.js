function Ejercicio15(){}

Ejercicio15.prototype.grid_generator = function(n){
    var a_grid = []
    for (var row = 0; row <= 20; row++){
        var a_row = []
        for (var col = 0; col <= 20; col++){
            if (row == 0 ||col == 0){
                a_row.push(1)
            } else{
                var s1 = a_grid[row-1][col]
                var s2 = a_row[col-1]
                var res = s1+s2
                a_row.push(res)
            }

        }
        a_grid.push(a_row)
    }
    // console.log(a_grid)
    return a_grid
}


Ejercicio15.prototype.getter = function(){
    var a_grid = this.grid_generator();
    var a_last_row = a_grid[a_grid.length-1];
    var n_result = a_last_row[a_last_row.length-1]
    console.log(n_result)
    return n_result
}

module.exports = Ejercicio15