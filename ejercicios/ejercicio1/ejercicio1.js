function Ejercicio1() {}

Ejercicio1.prototype.is_multiple = function(number){
    if (number%3 == 0 || number%5 == 0){
        return number;
    }
    return 0;
}

Ejercicio1.prototype.get_number = function(){
    var sum = 0;
    for (var i = 0; i < 1000; i++){
        var n_numero = this.is_multiple(i);
        sum += n_numero;
    }
    return sum;
}

module.exports = Ejercicio1