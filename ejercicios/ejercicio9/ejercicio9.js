function Ejercicio9(){};

Ejercicio9.prototype.get_pythagorean_triplet = function () {
    var suma = 1000;
    var a;
    var a_res = []
    for (a=1; a <= suma/3; a++){
        for (var b = a+1; b <= suma/2; b++){
            var c = suma - a - b;
            if(Math.pow(a,2)+Math.pow(b,2) == Math.pow(c,2)){
                a_res.push(a);
                a_res.push(b);
                a_res.push(c);
            }
        }
    }
    return a_res;
}

Ejercicio9.prototype.get_product = function(a_number){
    var n_result = 1;
    for(var i = 0; i < a_number.length; i++){
        n_result *= a_number[i];
    }
    return n_result;
}

module.exports = Ejercicio9