function Ejercicio4(){};

Ejercicio4.prototype.is_palindrome = function(n){
    var original = n.toString();
    var reversed = original.split('').reverse().join('')
    if(original == reversed){
        return true;
    }else{
        return false;
    }

}

Ejercicio4.prototype.iterator = function(){
    var n_max = 0;
    for(var i = 100; i < 1000; i++){
        // console.log(i);
        for(var j=100; j < 1000; j++){
            var n_number = this.is_palindrome(i*j);
            if(n_number){
                if(i*j > n_max){
                    n_max = i*j;
                }
                console.log(i*j+" ----> "+n_number);
            }

        }


    }
    console.log(n_max)
    return n_max;
}

module.exports = Ejercicio4