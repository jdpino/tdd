function Ejercicio12(){}

//Cuando intentamos hacerlo con los primeros valores nos damos cuenta de que es una función del tipo y=ax²+bx+c
//Al sustituir x=0 e y=0, 0=a*0²+n*0+c => c = 0 ------> y=ax²+bx
//Al sustituir x=2 y=3 (valores de la gráfica) -> 3=4a+2b => (3-2b)/4=a
//En x=3 y=6, por tanto -> 6=(3-2b/4)*3²+3b => 6=(27-18b)/4+3b => 6=27-18b+12b => 24=27-18b+12b => 24=27-6b => (24-27)/-6=b=1/2
//a=(3-2b)/4 b=1/2 => a=(3-2*(1/2))/4 => a= 2/4 = 1/2
//Con todo esto sacamos:
// y=ax²+bx => y = (1/2)*x²+(1/2)x => y=(1/2*x)x => y = (x(x+1))/2

Ejercicio12.prototype.get_number = function(){
    var number=0;
    var x=1;
    while(number===0){

        var count=0;
        var trinum= (x*(x+1))/2;

        for(var i=1;i<=Math.sqrt(trinum);i++){
            if(trinum%i===0){             //busca multiplos, si encuentra un múltiplo
                count+=2;                   // y es menor que la raiz cuadrada entonces hay un numero mas grande que la raiz
            }
        }

        if(count>500){
            number=trinum;
            break;
        }

        x+=1;
        // console.log(count,trinum,n);
    }
    // console.log(number);
    return number;
}

module.exports = Ejercicio12