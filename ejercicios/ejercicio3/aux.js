module.exports = {
    get_greater_number: function(n_number){
        var n_result = 0;
        for(var i = 0; i<n_number.length; i++){
            if (n_number[i] > n_result){
                n_result = n_number[i];
            }
        };

        return n_result;
    }
}