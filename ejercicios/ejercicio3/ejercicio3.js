var aux = require('./aux');

function Ejercicio3(){};

Ejercicio3.prototype.get_prime_numbers= function(){
    var n_number = 600851475143;
    var n_primo = 2;
    var a_primes = [];
    var counter = 0;

    while(n_number > 1){
        counter = 0;
        while ((n_number%n_primo) == 0){
            ++counter;
            n_number /= n_primo;
        }
        if (counter > 0){
            a_primes.push(n_primo)
        }
        n_primo++;
    }

    return a_primes
}

Ejercicio3.prototype.get_number= function(a_primes){
    var n_number = aux.get_greater_number(a_primes);
    return n_number;
}

module.exports = Ejercicio3