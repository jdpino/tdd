function Ejercicio14(){};


Ejercicio14.prototype.operator = function(n_number) {
    var result = [n_number]
    var i = n_number
    while (i != 1) {
        // console.log(i)
        if (i % 2 == 0) {
            i /= 2
        } else {
            i = (3 * i) + 1
        }
        result.push(i)
    }
    // console.log(result)
    return result.length
}

Ejercicio14.prototype.iterator = function() {
    var n_number = 0
    var n_chain = 0
    for (var i = 1; i < 1000000; i++) {
        console.log(i)
        var result = this.operator(i)
        if (result > n_chain) {
            n_chain = result
            n_number = i
        }
    }
    // console.log(n_number)
    return n_number
}

module.exports = Ejercicio14