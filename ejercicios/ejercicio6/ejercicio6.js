function Ejercicio6 () {}

Ejercicio6.prototype.get_sum_squares = function(){
    var result = 0;
    for(var i = 0; i <= 100; i++){
        result += Math.pow(i,2);
    }
    return result
}

Ejercicio6.prototype.get_square_sum = function(){
    var result = 0;
    for (var i = 0; i <= 100; i++){
        result += (i)
    }
    result = Math.pow(result, 2);
    return result
}

Ejercicio6.prototype.get_difference = function(){
    var sq_sum = this.get_square_sum();
    var sum_sq = this.get_sum_squares();
    var result = sq_sum - sum_sq;
    return result
}

module.exports = Ejercicio6